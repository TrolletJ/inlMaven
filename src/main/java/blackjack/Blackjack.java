package blackjack;


	import java.util.ArrayList;
	import java.util.List;
	import java.util.Scanner;

	public class Blackjack {
		static public Blackjack instance = new Blackjack();
		private Deck deck = new Deck();
		private List<Card> hand = new ArrayList<Card>();
		private int total;
		private int dealerTotal;
		private Scanner in = new Scanner(System.in);
		private String message = "Do you wanna play again? \nYes = y \nNo = n";
		private String answer = "";

		private Blackjack() {
			hand.add(deck.draw());
		}

		static public Blackjack getInstance() {
			if (instance == null) {
				instance = new Blackjack();
			}
			return instance;
		}

		public void hit() {
			Card card = deck.draw();
	
			total += score(card);
			System.out.println("hit: " +getRank(card)+ " "+card.getSuit());
			System.out.println(total);
			winCondition(total);
			
		}
		public String getRank(Card card) {
			
			switch(card.getValue()) {
			case 1: return "A";
			case 11: return"J";
			case 12: return"Q";
			case 13: return"K";
			
			}
			return String.valueOf(card.getValue());
		}

		public void stand() {

			if(total<15) {
				System.out.println("Hmph, chicken.");
			}
			while(total>dealerTotal) {
				Card dealer = deck.draw();
				System.out.println("Dealer: " + getRank(dealer)+ " "+dealer.getSuit());
				dealerTotal += score(dealer);
				System.out.println(dealerTotal);
			}

			winner(total, dealerTotal);
			
		}

		public void reset() {
			total = 0;
			dealerTotal = 0;
			deck = new Deck();
			Card c1 = deck.draw();
			Card c2 = deck.draw();
			total += score(c1) + score(c2);
			
			System.out.println(getRank(c1) + " "+ c1.getSuit());
			System.out.println(getRank(c2) + " "+ c2.getSuit());
			System.out.println("total: " + total);
			winCondition(total);
		}

		public int score(Card value) {
		int points = 0;
			switch (value.getValue()) {
			case 1:  points += 11; break;
			case 2:  points += 2; break;
			case 3:  points += 3; break;
			case 4:  points += 4; break;
			case 5:  points += 5;break;
			case 6:  points += 6;break;
			case 7:  points += 7;break;
			case 8:  points += 8;break;
			case 9:  points += 9;break;
			case 10: case 11: case 12: case 13:  points += 10;break;
			}

			return points;


		}
		private void winCondition(int score) {
			
			if(score == 21) {
				System.out.println("You're awesome! You got DAT BlackJack");
				newGame();
			}
			else if(score > 21) {
				System.out.println("Ehm... I think you picked the wrong table.");
				
				newGame();
			}
		}
		private void winner(int playerTotal, int dealerTotal) {
			if(playerTotal<=dealerTotal&& 21>dealerTotal) {
				System.out.println("The house wins LuLz!");
				newGame();
			}
			else if(playerTotal>dealerTotal||dealerTotal>21) {
				System.out.println("Wow, you're sooo good...");
				newGame();
			}
		}
		private void newGame() {
			System.out.println(message);
			answer = in.nextLine();
			if(answer.toLowerCase().equals("y")) {
				reset();
			}
			else if (answer.toLowerCase().equals("n")){
				System.out.println("Go f*** yourself");
				System.exit(0);
			}
			else {
				System.out.println("Are you high? GTFO...");
				System.exit(0);
			}
		}
	}




