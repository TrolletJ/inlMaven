package blackjack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
	private List<Card> cards;

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public Deck() {
		this.cards = new ArrayList<Card>();
		createCards();
	}

	public void createCards() {

		for (Suit cardSuit : Suit.values()) {
			for (int i = 1; i <= 13; ++i) {
				cards.add(new Card(i, cardSuit));
			}
		}
//		shuffle();

		
	}

	public Card draw() {
		Card temp = cards.get(0);
		cards.remove(0);
		return temp;
	}

	public void shuffle() {

		Collections.shuffle(cards);
	}
}

