package blackjack;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
// Joellas tester
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
class BlackjackTest {
	Blackjack blackjack;
	@BeforeEach
	void resetBlackjack() {
		blackjack = Blackjack.getInstance();
	}

	@Test
	void testScore1() {
		Card card1 = new Card(1, Suit.SPADES);
		Card card2 = new Card(1, Suit.CLUBS);
		assertEquals(22, blackjack.score(card1)+blackjack.score(card2), "2 ess blir 22");
	}
	
	@Test
	void testScore2() {
		Card card1 = new Card(9, Suit.SPADES);
		Card card2 = new Card(10, Suit.SPADES);
		assertEquals(19, blackjack.score(card1)+blackjack.score(card2), "9 och 10 �r 19");
	}
	@Test
	void testScore3() {
		Card card1 = new Card(11, Suit.SPADES);
		Card card2 = new Card(10, Suit.SPADES);
		assertEquals(20, blackjack.score(card1)+blackjack.score(card2), "knekt och 10 �r 20");
	}
	@Test
	void testScore4() {
		Card card1 = new Card(12, Suit.SPADES);
		Card card2 = new Card(13, Suit.SPADES);
		assertEquals(20, blackjack.score(card1)+blackjack.score(card2), "dam och kung �r 20");
	}
	@Test
	void testScore5() {
		Card card1 = new Card(1, Suit.SPADES);
		Card card2 = new Card(13, Suit.SPADES);
		assertEquals(21, blackjack.score(card1)+blackjack.score(card2), "ess och knekt �r 21");
	}
	@Test
	void testScore6() {
		Card card1 = new Card(1, Suit.SPADES);
		Card card2 = new Card(2, Suit.CLUBS);
		assertEquals(13, blackjack.score(card1)+blackjack.score(card2), "2 och ess blir 13");
	}
	@ParameterizedTest
	@ValueSource(ints = {0, 1, 2, 3})
	void testGetRank(int i) {
		Suit a =Suit.values()[i];
		Card card = new Card(1, a);
		
		assertNotNull(card);
	}
	

}
