package blackjack;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

// Joellas tester
public class DeckTest {
	Deck deck;
	@BeforeEach
	void resetDeck(){
		deck = new Deck();
		
	}
	@Test
	void testShuffle() {
		
		assertNotNull(deck.getCards());
		assertEquals(52, deck.getCards().size(), "deck size is 52");
		deck.shuffle();
		assertEquals(52, deck.getCards().size(), "deck size is 52");
		
	}
	@RepeatedTest(52)
	void testDrawShuffle() {
		deck.draw();
		assertEquals(51, deck.getCards().size(), "deck size is 52");
		deck.shuffle();
		assertEquals(51, deck.getCards().size(), "deck size is 52");
		
	}
	@Test
	void testEqualCards() {
		
		while(deck.getCards().size() > 0) {
			Card card = deck.draw();	
			for(int i = 0; i <deck.getCards().size(); i++) {
				assertFalse(card.equals(deck.getCards().get(i)));
				
			}
			
		}
	}
	
	

}
